package ru.tsc.anaumova.tm.exception.entity;

import ru.tsc.anaumova.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}