package ru.tsc.anaumova.tm.exception.field;

import ru.tsc.anaumova.tm.exception.AbstractException;

public class ExistsEmailException extends AbstractException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}